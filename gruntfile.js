module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          'sky/static/css/site.css': 'sky/static/less/site.less'
        }
      }
    },
    hogan: {
      mytarget : {
        src : 'sky/templates/**/*.hogan',
        dest : 'sky/static/js/templates.js',
        options : { binderName: 'hulk' }
      }
    },
    jshint: {
      options: { 
        jshintrc: true
      },
      all: ['sky/static/js/**/*.js']
    },
    watch: {
      styles: {
        files: ['sky/static/less/**/*.less'],
        tasks: ['less'],
        options: {
          nospawn: true
        },
        scripts: {
          files: ['sky/templates/**/*.hogan'],
          tasks: ['hogan'],
          options: {
            nospawn: true
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-hogan');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.registerTask('default', ['less', 'hogan', 'jshint', 'watch']);
};