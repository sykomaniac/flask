This project uses Flask (http://flask.pocoo.org/) - a python microframework, as an application server. Once logged in the app utilises Backbone.JS to implement a dynamic SPA.

Steps to run project:

The following guide assumes that you are trying to run this project on Windows. If this is not the case please make the relevant changes where necessary.

1. Install the latest version of Python 2 from: https://www.python.org/downloads/release/python-279/
2. Once this has been installed add the installation folder (e.g. C:/Python) to the Windows system path
3. Download EZ_setup from: https://bootstrap.pypa.io/ez_setup.py
4. Once downloaded install it by opening a command prompt, navigate to the download folder and running the file i.e. python ez_setup.py
4. After installation completes add the python scripts (e.g. C:/Python/Scripts) folder to the Windows system path
5. From a command prompt install pip by typing "install pip" (without the quotes)
6. Once pip is installed install the project dependencies by typing the following command "pip install Flask Flask-Login Flask-SQLAlchemy" (without the quotes)
7. In the command prompt change into the checkout directory
8. In the command prompt type "python server.py" (without the quotes)
9. Assuming the above steps completed successfully the application will now be available at http://localhost:5000

This project uses grunt to continuously compile templates and less files. You will need to complete the relevant set-up for this to work.
This project also uses bower to install any web dependencies e.g. JQuery, Backbone etc. However to reduce the need to install additional packages the distribution files have been copied to the relevant static folders (previous imports are commented out in the source code).

Credentials:

When running the app you will need to enter credentials to login. They are as follows:

Email: bill@bill.com
Password: password

TODO:

With more time the following tasks will be completed:

1. Make use of an AMD library e.g. RequireJS
2. Experiment how to access Flask variables in JavaScript