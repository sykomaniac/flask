from flask import Flask, url_for
from flask.ext.login import (LoginManager, UserMixin)
from flask.ext.sqlalchemy import SQLAlchemy
import os
import logging

sky = Flask(__name__)

# Logging
logHandler = logging.FileHandler('test.log')
logHandler.setLevel(logging.INFO)
sky.logger.addHandler(logHandler)
sky.logger.setLevel(logging.INFO)

# Flask login
login_manager = LoginManager()
login_manager.init_app(sky)

# App config
sky.secret_key = 'c474e681-5421-4180-ba28-027dae7d4536'
sky.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

# Database
db = SQLAlchemy(sky)

# Function to easily find your assets
# In your template use <link rel=stylesheet href="{{ static('filename') }}">
sky.jinja_env.globals['static'] = (
    lambda filename: url_for('static', filename = filename)
)

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, index=True)
    password = db.Column(db.String(50))

    def __init__(self, email, password):
        self.email = email
        self.password = password
    
    def is_authenticated(self):
        return True
 
    def is_active(self):
        return True
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % self.email

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))
	
from sky import views