from flask import (render_template, flash, redirect, url_for, request, get_flashed_messages)
from sky import (sky, User)
from flask.ext.login import (login_user, logout_user, login_required)

@sky.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('login.html')
    email = request.form['email']
    password = request.form['password']
    # validate username and password
    user = User.query.filter_by(email=email, password=password).first()
    if user is None:
        flash('Email or Password is invalid', 'error')
        return redirect(url_for('index'))
    login_user(user)
    return redirect(url_for('home'))

@sky.route('/app/home')
@sky.route('/app/<path>')
@login_required
def home(path='NULL'):
    return render_template('index.html')
	
@sky.route('/app/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))