var app = app || {};

$(function(){
    
    //Calls
    app.Call = Backbone.AssociatedModel.extend({
        defaults: {
            called: {},
            duration: {},
            cost: {}
        }
    });
    
    app.CallCharges = Backbone.AssociatedModel.extend({
        defaults: {
            calls: [],
            total: 0,
        },
        relations: [
            {
                relatedModel: app.Call,
                key: 'calls',
                type: Backbone.Many
            }
         ]
    });
    
    //Packages
    app.Subscription = Backbone.AssociatedModel.extend({
        defaults: {
            type: "",
            name: "",
            cost: 0
        }
    });
    
    app.Packages = Backbone.AssociatedModel.extend({
        defaults: {
            subscriptions: [],
            total: 0
        },
        relations: [
            {
                relatedModel: app.Subscription,
                key: 'subscriptions',
                type: Backbone.Many
            }
         ]
    });
    
    //Sky store    
    app.StoreItem = Backbone.AssociatedModel.extend({
        defaults: {
            type: {},
            title: {},
            cost: {}
        }
    });
    
    app.SkyStore = Backbone.AssociatedModel.extend({
        defaults: {
            buyAndKeep: [],
            rentals: [],
            total: 0
        },
        relations: [
            {
                relatedModel: app.StoreItem,
                key: 'buyAndKeep',
                type: Backbone.Many
            },
            {
                relatedModel: app.StoreItem,
                key: 'rentals',
                type: Backbone.Many
            }
         ]
    });
    
    //Statement
    app.Statement = Backbone.AssociatedModel.extend({
        defaults: {
            due: new Date(),
            generated: new Date(),
            period: {},
            total: 0
        }
    });
    
    app.Bill = Backbone.AssociatedModel.extend({
        url: 'http://safe-plains-5453.herokuapp.com/bill.json',
        defaults: {
            callCharges: {},
            package: {},
            skyStore: {},
            statement: {},
            total: 0
        },
        relations: [
            {
                relatedModel: app.CallCharges,
                key: 'callCharges',
                type: Backbone.One
            },
            {
                relatedModel: app.Packages,
                key: 'package',
                type: Backbone.One
            },
            {
                relatedModel: app.SkyStore,
                key: 'skyStore',
                type: Backbone.One
            },
            {
                relatedModel: app.Statement,
                key: 'statement',
                type: Backbone.One
            }
         ]
    });
});