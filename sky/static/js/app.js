var app = app || {};

$(function () {
    var title = $('#main h1'), content = $('.content');
    
    app.ViewManager = {
        currentView : null,
        showView : function(view) {
            if (this.currentView !== null && this.currentView.cid !== view.cid) {
                this.currentView.remove();
            }
            this.currentView = view;
            title.text(view.pageTitle);
            return content.append(view.render().el);
        }
    };

    app.WorkspaceRouter = Backbone.Router.extend({
        routes: {
            "home": "dashboard",
            "bills": "bills"
        },

        dashboard: function () {
            app.ViewManager.showView(new app.DashboardView());
        },

        bills: function () {
            var aBill = new app.Bill();
            aBill.fetch({
                success: function(model) {
                    app.ViewManager.showView(new app.BillView({model: model}));
                }
            });
        }
    });

    // Create router
    new app.WorkspaceRouter();
    
    // Start history
    Backbone.history.start({
        pushState: true,
        root: 'app/'
    });
    
    //Override anchor clicks so router is used
    $('#layout').on('click', 'a:not([data-bypass],[target])', function (evt) {
        var href = $(this).attr('href');

        if (href === '#') {
            evt.preventDefault();
        }

        // Don't prevent the user opening content in a new tab
        if (evt.metaKey || evt.ctrlKey) {
            return;
        }

        evt.preventDefault();
        Backbone.history.navigate(href, { trigger: true });
    });
});