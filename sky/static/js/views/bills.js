var app = app || {};

$(function(){
    app.BillView = Backbone.View.extend({
        tagName: 'div',
        pageTitle: 'Bills',

        initialize: function () {
            var template = templates['bill'];
            this.$el.html(template.render(this.model.toJSON()));
        },
        
        events: {
          "click .close" : "close",
          "click .view" : "show"
        },
                                        
        close: function() {
            this.$('.statement').fadeOut();
        },
            
        show: function() {
            var stmt = this.$('.statement');
            if (! stmt.is(':visible')) {
                stmt.fadeIn('slow');   
            }
        }
    });
});