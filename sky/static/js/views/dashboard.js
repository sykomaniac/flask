var app = app || {};

$(function(){
    app.DashboardView = Backbone.View.extend({
        tagName: 'div',
        pageTitle: 'Dashboard',

        initialize: function () {
            var template = templates['dashboard'];
            this.$el.html(template.render());
        }
    });
});